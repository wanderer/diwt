// Copyright 2022 wanderer <wanderer at dotya.ml>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"encoding/csv"
	"log"
	"os"
	"strings"
)

var csvreader *csv.Reader

// readFile tries to open the file passed in func argument and returns a
// *csvreader if the file could be open.
func readFile(fname string) csv.Reader {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatalf("error reading file %s, bailing...\n", fname)
	}

	csvreader = csv.NewReader(f)

	return *csvreader
}

// readArg returns a *csvreader that reads from the passed arg.
func readArg(arg string) csv.Reader {
	if arg == "" {
		log.Fatalf("the arg is empty, bailing...\n")
	}

	return *csv.NewReader(strings.NewReader(arg))
}
