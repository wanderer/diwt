// Copyright 2022 wanderer <wanderer at dotya.ml>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import "testing"

func TestReadArg(t *testing.T) {
	want := [][]string{{"whatever"}}
	r := readArg("whatever")

	got, err := r.ReadAll()
	if err != nil {
		t.Error("error reading arg", err)
	}

	if got[0][0] != want[0][0] {
		t.Errorf("output mismatch, want: %q, got %q", want, got)
	}
}
