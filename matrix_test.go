// Copyright 2022 wanderer <wanderer at dotya.ml>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import "testing"

func TestStoi(t *testing.T) {
	want := 42
	got := stoi("42")

	if got != want {
		t.Errorf("output mismatch, want: %d, got %d", want, got)
	}
}

func TestRowtoi(t *testing.T) {
	row := []string{"42", "15", "39", "1270880", "11", "0", "-342"}
	want := []int{42, 15, 39, 1270880, 11, 0, -342}
	got := rowtoi(row)

	if len(want) != len(got) || len(got) != len(row) {
		t.Errorf(
			"row len mismatch, len(row): %d, len(want): %d, len(got): %d",
			len(row), len(want), len(got),
		)
	}

	for i := range row {
		if want[i] != got[i] {
			t.Errorf("conversion gone wrong, want: %#v, got: %#v", want, got)
		}
	}
}

func TestIsNonEmpty(t *testing.T) {
	strm := [][]string{{}}

	want := false
	got := isNonEmptyMatrix(strm)

	if want != got {
		t.Errorf("matrix eval gone wrong, want: %t, got: %t, matrix: %#v",
			want, got, strm,
		)
	}

	strm = [][]string{{"905548", "28383", "78439", "878237892"}}
	want = true
	got = isNonEmptyMatrix(strm)

	if want != got {
		t.Errorf("matrix eval gone wrong, want: %t, got: %t, matrix: %#v",
			want, got, strm,
		)
	}

	strm = [][]string{{"75420", "59854"}, {"4532", "543"}}

	got = isNonEmptyMatrix(strm)
	if want != got {
		t.Errorf("matrix eval gone wrong, want: %t, got: %t, matrix: %#v",
			want, got, strm,
		)
	}
}

func TestInitMatrix(t *testing.T) {
	strm := [][]string{{"78429", "43", "28383", "78439", "878237892"}, {"542", "1566", "9", "76982", "4323"}}
	want := 2
	got := initMatrix(strm)

	if want != len(got.rows) || len(got.rows) != len(strm) {
		t.Errorf("matrix len mismatch, want: %d, got: %d, strm: %d",
			want, len(got.rows), len(strm),
		)
	}
}

func TestConvertMatrix(t *testing.T) {
	strm := [][]string{{"78429", "43", "28383", "78439", "878237892"}, {"542", "1566", "9", "76982", "4323"}}
	want := &matrix{}
	got := convertMatrix(strm)

	want.rows = [][]int{{78429, 43, 28383, 78439, 878237892}, {542, 1566, 9, 76982, 4323}}

	if len(want.rows) != len(got.rows) || len(got.rows) != len(strm) {
		t.Errorf("matrix len mismatch, want: %d, got: %d, strm: %d",
			len(want.rows), len(got.rows), len(strm),
		)
	}

	for i, row := range strm {
		for j := 0; j < len(row); j++ {
			if want.rows[i][j] != got.rows[i][j] {
				t.Errorf(
					"conversion gone wrong, want: %#v, got: %#v, strm: %#v",
					want, got, strm,
				)
			}
		}
	}
}

func TestFmtPath(t *testing.T) {
	want := "[43,42] (98732)"
	got := fmtPath(43, 42, 98732)

	if want != got {
		t.Errorf("string mismatch, want: %q, got: %q", want, got)
	}
}
