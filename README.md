# [`diwt`](https://git.dotya.ml/wanderer/diwt)

[![built with nix](./.badges/built-with-nix.svg)](https://builtwithnix.org)
[![pre-commit](./.badges/pre-commit-enabled.svg)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer/diwt/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer/diwt)
[![Go Report Card](https://goreportcard.com/badge/git.dotya.ml/wanderer/diwt)](https://goreportcard.com/report/git.dotya.ml/wanderer/diwt)
[![Go Reference](./.badges/pkggodev.svg)](https://pkg.go.dev/git.dotya.ml/wanderer/diwt)

> note: don't ask about the name...

[![asciicast](https://asciinema.org/a/WOmNcm3UtkrfpRrBmTPAz6W23.svg)](https://asciinema.org/a/WOmNcm3UtkrfpRrBmTPAz6W23)

# LICENSE
GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
