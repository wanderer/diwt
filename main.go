// Copyright 2022 wanderer <wanderer at dotya.ml>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
)

const (
	mFlagDescr = "path to a CSV file containing matrix values"
	aFlagDescr = "read matrix from stdin-provided arg"
	usage      = `Usage of diwt:
  -m, --matrixfile ` + mFlagDescr + `
  -a, --stdin ` + aFlagDescr + `
  -h, --help prints help information
`
)

var version = "development"

func main() {
	log.Println("starting diwt, version", version)

	var f string

	var a string

	flag.StringVar(&f, "matrixfile", "", mFlagDescr)
	flag.StringVar(&f, "m", "", mFlagDescr)
	flag.StringVar(&a, "stdin", "", aFlagDescr)
	flag.StringVar(&a, "a", "", aFlagDescr)

	flag.Usage = func() { fmt.Fprint(os.Stderr, usage) }

	flag.Parse()

	var r csv.Reader

	switch {
	case f == "" && a == "":
		log.Println("no file provided, bailing...")
		os.Exit(3)

	case a != "" && f != "":
		log.Println(
			"only provide one of the flags (file or stdin), not both. " +
				"exiting.",
		)
		os.Exit(4)

	case f != "":
		r = readFile(f)

	case a != "":
		r = readArg(a)
	}

	records, err := r.ReadAll()
	if err != nil {
		log.Fatalln("error reading records:", err)
	}

	m := convertMatrix(records)

	traverseMatrixMinSumPath(m)

	log.Println("looks like we're done here.")
}
